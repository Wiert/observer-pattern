<?php

namespace ObserverPattern;

interface ISubscriber {

	public function notify();

}