<?php

namespace ObserverPattern;

class YoutubeChannel {

	private $subscribers = array();

	public function subscribe(ISubscriber $s){

		$this->subscribers[] = $s;

	}

	public function unSubscribe(ISubscriber $s){
			
			$value = array_search($s, $this->subscribers);
			unset($this->subscribers[$value]);
			echo "Subscriber unsubscribed<br>";
	
	}

	public function notifySubscribers(){
		
		foreach ($this->subscribers as $subscriber){
			
			$subscriber->notify();

		}

	}

}