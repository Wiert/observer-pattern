<?php

namespace ObserverPattern;
include 'YoutubeChannel.php';
include 'ISubscriber.php';
include 'User.php';
include 'Moderator.php';

class Observing {

	function __construct(){
	
		$mychannel = new YoutubeChannel;

		$wiert = new User;

		$pim = new Moderator;

		$mychannel->subscribe($wiert);

		$mychannel->subscribe($pim);

		$mychannel->notifySubscribers();

		$mychannel->unSubscribe($pim);
		
		$mychannel->notifySubscribers();		

	}

}

$observe = new Observing;