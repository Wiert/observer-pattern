<?php

namespace ObserverPattern;

class User implements ISubscriber {

	public function notify(){

		echo "User notified!<br>";

	}

}