<?php

namespace ObserverPattern;

class Moderator implements ISubscriber {

	public function notify(){

		echo "Moderator notified!<br>";

	}

}